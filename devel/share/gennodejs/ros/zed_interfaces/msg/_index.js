
"use strict";

let Skeleton2D = require('./Skeleton2D.js');
let Keypoint3D = require('./Keypoint3D.js');
let Keypoint2Df = require('./Keypoint2Df.js');
let BoundingBox2Df = require('./BoundingBox2Df.js');
let BoundingBox3D = require('./BoundingBox3D.js');
let ObjectsStamped = require('./ObjectsStamped.js');
let BoundingBox2Di = require('./BoundingBox2Di.js');
let RGBDSensors = require('./RGBDSensors.js');
let Keypoint2Di = require('./Keypoint2Di.js');
let Skeleton3D = require('./Skeleton3D.js');
let Object = require('./Object.js');

module.exports = {
  Skeleton2D: Skeleton2D,
  Keypoint3D: Keypoint3D,
  Keypoint2Df: Keypoint2Df,
  BoundingBox2Df: BoundingBox2Df,
  BoundingBox3D: BoundingBox3D,
  ObjectsStamped: ObjectsStamped,
  BoundingBox2Di: BoundingBox2Di,
  RGBDSensors: RGBDSensors,
  Keypoint2Di: Keypoint2Di,
  Skeleton3D: Skeleton3D,
  Object: Object,
};
