// Auto-generated. Do not edit!

// (in-package arm_base.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class joint_angular {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.waist = null;
      this.shoulder = null;
      this.elbow = null;
      this.wrist = null;
      this.life_time = null;
    }
    else {
      if (initObj.hasOwnProperty('waist')) {
        this.waist = initObj.waist
      }
      else {
        this.waist = 0.0;
      }
      if (initObj.hasOwnProperty('shoulder')) {
        this.shoulder = initObj.shoulder
      }
      else {
        this.shoulder = 0.0;
      }
      if (initObj.hasOwnProperty('elbow')) {
        this.elbow = initObj.elbow
      }
      else {
        this.elbow = 0.0;
      }
      if (initObj.hasOwnProperty('wrist')) {
        this.wrist = initObj.wrist
      }
      else {
        this.wrist = 0.0;
      }
      if (initObj.hasOwnProperty('life_time')) {
        this.life_time = initObj.life_time
      }
      else {
        this.life_time = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type joint_angular
    // Serialize message field [waist]
    bufferOffset = _serializer.float64(obj.waist, buffer, bufferOffset);
    // Serialize message field [shoulder]
    bufferOffset = _serializer.float64(obj.shoulder, buffer, bufferOffset);
    // Serialize message field [elbow]
    bufferOffset = _serializer.float64(obj.elbow, buffer, bufferOffset);
    // Serialize message field [wrist]
    bufferOffset = _serializer.float64(obj.wrist, buffer, bufferOffset);
    // Serialize message field [life_time]
    bufferOffset = _serializer.float64(obj.life_time, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type joint_angular
    let len;
    let data = new joint_angular(null);
    // Deserialize message field [waist]
    data.waist = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [shoulder]
    data.shoulder = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [elbow]
    data.elbow = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [wrist]
    data.wrist = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [life_time]
    data.life_time = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 40;
  }

  static datatype() {
    // Returns string type for a message object
    return 'arm_base/joint_angular';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '707d1c84207d8c1d73ed766fa7a55b3c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float64 waist
    float64 shoulder
    float64 elbow
    float64 wrist
    float64 life_time
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new joint_angular(null);
    if (msg.waist !== undefined) {
      resolved.waist = msg.waist;
    }
    else {
      resolved.waist = 0.0
    }

    if (msg.shoulder !== undefined) {
      resolved.shoulder = msg.shoulder;
    }
    else {
      resolved.shoulder = 0.0
    }

    if (msg.elbow !== undefined) {
      resolved.elbow = msg.elbow;
    }
    else {
      resolved.elbow = 0.0
    }

    if (msg.wrist !== undefined) {
      resolved.wrist = msg.wrist;
    }
    else {
      resolved.wrist = 0.0
    }

    if (msg.life_time !== undefined) {
      resolved.life_time = msg.life_time;
    }
    else {
      resolved.life_time = 0.0
    }

    return resolved;
    }
};

module.exports = joint_angular;
