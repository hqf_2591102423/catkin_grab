;; Auto-generated. Do not edit!


(when (boundp 'arm_base::joint_angular)
  (if (not (find-package "ARM_BASE"))
    (make-package "ARM_BASE"))
  (shadow 'joint_angular (find-package "ARM_BASE")))
(unless (find-package "ARM_BASE::JOINT_ANGULAR")
  (make-package "ARM_BASE::JOINT_ANGULAR"))

(in-package "ROS")
;;//! \htmlinclude joint_angular.msg.html


(defclass arm_base::joint_angular
  :super ros::object
  :slots (_waist _shoulder _elbow _wrist _life_time ))

(defmethod arm_base::joint_angular
  (:init
   (&key
    ((:waist __waist) 0.0)
    ((:shoulder __shoulder) 0.0)
    ((:elbow __elbow) 0.0)
    ((:wrist __wrist) 0.0)
    ((:life_time __life_time) 0.0)
    )
   (send-super :init)
   (setq _waist (float __waist))
   (setq _shoulder (float __shoulder))
   (setq _elbow (float __elbow))
   (setq _wrist (float __wrist))
   (setq _life_time (float __life_time))
   self)
  (:waist
   (&optional __waist)
   (if __waist (setq _waist __waist)) _waist)
  (:shoulder
   (&optional __shoulder)
   (if __shoulder (setq _shoulder __shoulder)) _shoulder)
  (:elbow
   (&optional __elbow)
   (if __elbow (setq _elbow __elbow)) _elbow)
  (:wrist
   (&optional __wrist)
   (if __wrist (setq _wrist __wrist)) _wrist)
  (:life_time
   (&optional __life_time)
   (if __life_time (setq _life_time __life_time)) _life_time)
  (:serialization-length
   ()
   (+
    ;; float64 _waist
    8
    ;; float64 _shoulder
    8
    ;; float64 _elbow
    8
    ;; float64 _wrist
    8
    ;; float64 _life_time
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _waist
       (sys::poke _waist (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _shoulder
       (sys::poke _shoulder (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _elbow
       (sys::poke _elbow (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _wrist
       (sys::poke _wrist (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _life_time
       (sys::poke _life_time (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _waist
     (setq _waist (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _shoulder
     (setq _shoulder (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _elbow
     (setq _elbow (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _wrist
     (setq _wrist (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _life_time
     (setq _life_time (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get arm_base::joint_angular :md5sum-) "707d1c84207d8c1d73ed766fa7a55b3c")
(setf (get arm_base::joint_angular :datatype-) "arm_base/joint_angular")
(setf (get arm_base::joint_angular :definition-)
      "float64 waist
float64 shoulder
float64 elbow
float64 wrist
float64 life_time
")



(provide :arm_base/joint_angular "707d1c84207d8c1d73ed766fa7a55b3c")


