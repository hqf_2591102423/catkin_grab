
(cl:in-package :asdf)

(defsystem "arm_base-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "joint_angular" :depends-on ("_package_joint_angular"))
    (:file "_package_joint_angular" :depends-on ("_package"))
  ))