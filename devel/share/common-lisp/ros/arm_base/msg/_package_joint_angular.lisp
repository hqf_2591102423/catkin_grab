(cl:in-package arm_base-msg)
(cl:export '(WAIST-VAL
          WAIST
          SHOULDER-VAL
          SHOULDER
          ELBOW-VAL
          ELBOW
          WRIST-VAL
          WRIST
          LIFE_TIME-VAL
          LIFE_TIME
))