#include <gpd_ros/grasp_detection_node.h>


/** constants for input point cloud types */
const int GraspDetectionNode::POINT_CLOUD_2 = 0; ///< sensor_msgs/PointCloud2
const int GraspDetectionNode::CLOUD_INDEXED = 1; ///< cloud with indices
const int GraspDetectionNode::CLOUD_SAMPLES = 2; ///< cloud with (x,y,z) samples


GraspDetectionNode::GraspDetectionNode(ros::NodeHandle& node) : has_cloud_(false), has_normals_(false),
  size_left_cloud_(0), has_samples_(true), frame_(""), use_importance_sampling_(false), set_workspace_(false)
{
  printf("Init ....\n");
  cloud_camera_ = NULL;

  // set camera viewpoint to default origin
//  std::vector<double> camera_position;
//  node.getParam("camera_position", camera_position);
//  view_point_ << camera_position[0], camera_position[1], camera_position[2];

  // choose sampling method for grasp detection
//  node.param("use_importance_sampling", use_importance_sampling_, false);

//  if (use_importance_sampling_)
//  {
//    importance_sampling_ = new SequentialImportanceSampling(node);
//  }
  std::string cfg_file;
  node.param("config_file", cfg_file, std::string(""));
  grasp_detector_ = new gpd::GraspDetector(cfg_file);//在这把cfg文件传给gpd，然后得到工作空间
  printf("Created GPD ....\n");

  // Read input cloud and sample ROS topics parameters.
  int cloud_type;
  node.param("cloud_type", cloud_type, POINT_CLOUD_2);
  std::string cloud_topic;
  node.param("cloud_topic", cloud_topic, std::string("/camera/depth_registered/points"));
  std::string samples_topic;
  node.param("samples_topic", samples_topic, std::string(""));
  std::string rviz_topic;
  node.param("rviz_topic", rviz_topic, std::string("plot_grasps"));

  if (!rviz_topic.empty()) {
    grasps_rviz_pub_ = node.advertise<visualization_msgs::MarkerArray>(rviz_topic, 1);
    use_rviz_ = true;
  } else {
    use_rviz_ = false;
  }

  // subscribe to input point cloud ROS topic
  if (cloud_type == POINT_CLOUD_2) {
    cloud_sub_ = node.subscribe(cloud_topic, 1, &GraspDetectionNode::cloud_callback, this);
  } else if (cloud_type == CLOUD_INDEXED) {
    cloud_sub_ = node.subscribe(cloud_topic, 1, &GraspDetectionNode::cloud_indexed_callback, this);
  } else if (cloud_type == CLOUD_SAMPLES) {
    cloud_sub_ = node.subscribe(cloud_topic, 1, &GraspDetectionNode::cloud_samples_callback, this);
    //    grasp_detector_->setUseIncomingSamples(true);
    has_samples_ = false;
  }

  // subscribe to input samples ROS topic折射sample用的
  if (!samples_topic.empty()) {
    samples_sub_ = node.subscribe(samples_topic, 1, &GraspDetectionNode::samples_callback, this);
    has_samples_ = false;
  }

  //subscribe to input workspace from yolo detector
  workspace_sub_ = node.subscribe("/workspace", 1, &GraspDetectionNode::workspace_callback, this);


  // uses ROS topics to publish grasp candidates, antipodal grasps, and grasps after clustering 发布爪子用的
  grasps_pub_ = node.advertise<gpd_ros::GraspConfigList>("clustered_grasps", 10);

  rviz_plotter_ = new GraspPlotter(node, grasp_detector_->getHandSearchParameters().hand_geometry_);

  node.getParam("workspace", workspace_);//workspace的空间，这完全没用其实，需要在gpd本体里面改
  
  gpd_switch_service_=node.advertiseService("set_gpd_status",
                                            &GraspDetectionNode::gpdSwitchCallback,
                                            this);//这个不用加，是因为运行的时候会自己加前缀
  gpd_enable_flag_=true;
}


void GraspDetectionNode::run()
{
  ros::Rate rate(100);
  ROS_INFO("Waiting for point cloud to arrive ...");

  while (ros::ok()) {
    // if (!gpd_enable_flag_) {
    //   // ros::spinOnce();//好像确实不需要，当那边call这的时候，再处理
    //   // ROS_INFO("gpd disable ...");
    //   rate.sleep();
    //   continue;
    // }//这就直接不往下运行，减少算力
    //加一个标志位，6个数更新了，才进行
    if(set_workspace_){//如果没设置工作空间，就给退出循环
    if (has_cloud_) {//考虑这个一直是ture怎么办，两个标志位的时间戳
      // Detect grasps in point cloud.
      std::vector<std::unique_ptr<gpd::candidate::Hand>> grasps = detectGraspPoses();
      
      // Visualize the detected grasps in rviz.
      if (use_rviz_) {
        rviz_plotter_->drawGrasps(grasps, frame_);
      }

      // Reset the system.
      has_cloud_ = false;
      has_samples_ = false;
      has_normals_ = false;
      set_workspace_=false;//如果进循环了就初始化
      ROS_INFO("Waiting for point cloud to arrive ...");
    }
    }

    ros::spinOnce();
    rate.sleep();
  }
}


std::vector<std::unique_ptr<gpd::candidate::Hand>> GraspDetectionNode::detectGraspPoses()
{
  // detect grasp poses
  std::vector<std::unique_ptr<gpd::candidate::Hand>> grasps;

  if (use_importance_sampling_)
  {
//    cloud_camera_->filterWorkspace(workspace_);
//    cloud_camera_->voxelizeCloud(0.003);
//    cloud_camera_->calculateNormals(4);
//    grasps = importance_sampling_->detectGrasps(*cloud_camera_);
	  printf("Error: importance sampling is not supported yet\n");
  }
  else
  {
    // preprocess the point cloud
    grasp_detector_->preprocessPointCloud(*cloud_camera_);//在这步进行被关掉的
    // detect grasps in the point cloud
    grasps = grasp_detector_->detectGrasps(*cloud_camera_);//这就是获得的抓取序列，对这个进行更改
  }

  // Publish the selected grasps.发布抓取手，或许还可以发布抓取的点？
  gpd_ros::GraspConfigList selected_grasps_msg = GraspMessages::createGraspListMsg(grasps, cloud_camera_header_);//在这发布的话题，这个是个什么类型的值
  grasps_pub_.publish(selected_grasps_msg);//这个才是发布的话题手吗？从这得到位置和姿态就行
  ROS_INFO_STREAM("Published " << selected_grasps_msg.grasps.size() << " highest-scoring grasps.");

  return grasps;
}


std::vector<int> GraspDetectionNode::getSamplesInBall(const PointCloudRGBA::Ptr& cloud,
  const pcl::PointXYZRGBA& centroid, float radius)
{
  std::vector<int> indices;
  std::vector<float> dists;
  pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
  kdtree.setInputCloud(cloud);
  kdtree.radiusSearch(centroid, radius, indices, dists);
  return indices;
}


void GraspDetectionNode::cloud_callback(const sensor_msgs::PointCloud2& msg)
{
  if (!has_cloud_)//用的是这个，得跟workspace同步，这个最好是workspace之后，如果没有点云进入下面
  {
    if(!cloud_camera_)//如果不是空，删除掉这个内存
      delete cloud_camera_;//这是后加的，解决了内存泄露问题
    cloud_camera_ = NULL;//然后给设置为空
    if(set_workspace_){//如果设置了工作空间，就执行下面的
    Eigen::Matrix3Xd view_points(3,1);
    view_points.col(0) = view_point_;

    if (msg.fields.size() == 6 && msg.fields[3].name == "normal_x" && msg.fields[4].name == "normal_y"
      && msg.fields[5].name == "normal_z")
    {
      PointCloudPointNormal::Ptr cloud(new PointCloudPointNormal);
      pcl::fromROSMsg(msg, *cloud);
      cloud_camera_ = new gpd::util::Cloud(cloud, 0, view_points);
      cloud_camera_header_ = msg.header;
      ROS_INFO_STREAM("Received cloud with " << cloud_camera_->getCloudProcessed()->size() << " points and normals.");
    }
    else
    {
      PointCloudRGBA::Ptr cloud(new PointCloudRGBA);
      pcl::fromROSMsg(msg, *cloud);
      cloud_camera_ = new gpd::util::Cloud(cloud, 0, view_points);
      cloud_camera_header_ = msg.header;
      ROS_INFO_STREAM("Received cloud with " << cloud_camera_->getCloudProcessed()->size() << " points.");
    }

    has_cloud_ = true;
    frame_ = msg.header.frame_id;
    }
  }
}


void GraspDetectionNode::cloud_indexed_callback(const gpd_ros::CloudIndexed& msg)
{
  if (!has_cloud_)
  {
    initCloudCamera(msg.cloud_sources);

    // Set the indices at which to sample grasp candidates.
    std::vector<int> indices(msg.indices.size());
    for (int i=0; i < indices.size(); i++)
    {
      indices[i] = msg.indices[i].data;
    }
    cloud_camera_->setSampleIndices(indices);

    has_cloud_ = true;
    frame_ = msg.cloud_sources.cloud.header.frame_id;

    ROS_INFO_STREAM("Received cloud with " << cloud_camera_->getCloudProcessed()->size() << " points, and "
      << msg.indices.size() << " samples");
  }
}


void GraspDetectionNode::cloud_samples_callback(const gpd_ros::CloudSamples& msg)
{
  if (!has_cloud_)
  {
    initCloudCamera(msg.cloud_sources);

    // Set the samples at which to sample grasp candidates.
    Eigen::Matrix3Xd samples(3, msg.samples.size());
    for (int i=0; i < msg.samples.size(); i++)
    {
      samples.col(i) << msg.samples[i].x, msg.samples[i].y, msg.samples[i].z;
    }
    cloud_camera_->setSamples(samples);

    has_cloud_ = true;
    has_samples_ = true;
    frame_ = msg.cloud_sources.cloud.header.frame_id;

    ROS_INFO_STREAM("Received cloud with " << cloud_camera_->getCloudProcessed()->size() << " points, and "
      << cloud_camera_->getSamples().cols() << " samples");
  }
}


void GraspDetectionNode::samples_callback(const gpd_ros::SamplesMsg& msg)
{
  if (!has_samples_)
  {
    Eigen::Matrix3Xd samples(3, msg.samples.size());

    for (int i=0; i < msg.samples.size(); i++)
    {
      samples.col(i) << msg.samples[i].x, msg.samples[i].y, msg.samples[i].z;
    }

    cloud_camera_->setSamples(samples);
    has_samples_ = true;

    ROS_INFO_STREAM("Received grasp samples message with " << msg.samples.size() << " samples");
  }
}

void GraspDetectionNode::workspace_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  // ROS_INFO("Had recevie workspace");
  if(!set_workspace_){
  // ROS_INFO("Had recevie workspace1");
  workspace_=msg->data;
  grasp_detector_->setworkspace(workspace_);
  // ROS_INFO("Had recevie workspace2");
  set_workspace_=true;
  }
}

bool GraspDetectionNode::gpdSwitchCallback(std_srvs::SetBoolRequest &request, std_srvs::SetBoolResponse &response) 
{
  gpd_enable_flag_= request.data;//通过这个服务决定是否进行检测
  response.success = true;
  if (gpd_enable_flag_) response.message = "gpd enable!";
  else response.message = "gpd disable!";
  return true;
}



void GraspDetectionNode::initCloudCamera(const gpd_ros::CloudSources& msg)
{
  // clean up
  delete cloud_camera_;
  cloud_camera_ = NULL;

  // Set view points.
  Eigen::Matrix3Xd view_points(3, msg.view_points.size());
  for (int i = 0; i < msg.view_points.size(); i++)
  {
    view_points.col(i) << msg.view_points[i].x, msg.view_points[i].y, msg.view_points[i].z;
  }

  // Set point cloud.
  if (msg.cloud.fields.size() == 6 && msg.cloud.fields[3].name == "normal_x"
    && msg.cloud.fields[4].name == "normal_y" && msg.cloud.fields[5].name == "normal_z")
  {
    PointCloudPointNormal::Ptr cloud(new PointCloudPointNormal);
    pcl::fromROSMsg(msg.cloud, *cloud);

    // TODO: multiple cameras can see the same point
    Eigen::MatrixXi camera_source = Eigen::MatrixXi::Zero(view_points.cols(), cloud->size());
    for (int i = 0; i < msg.camera_source.size(); i++)
    {
      camera_source(msg.camera_source[i].data, i) = 1;
    }

    cloud_camera_ = new gpd::util::Cloud(cloud, camera_source, view_points);
  }
  else
  {
    PointCloudRGBA::Ptr cloud(new PointCloudRGBA);
    pcl::fromROSMsg(msg.cloud, *cloud);

    // TODO: multiple cameras can see the same point
    Eigen::MatrixXi camera_source = Eigen::MatrixXi::Zero(view_points.cols(), cloud->size());
    for (int i = 0; i < msg.camera_source.size(); i++)
    {
      camera_source(msg.camera_source[i].data, i) = 1;
    }

    cloud_camera_ = new gpd::util::Cloud(cloud, camera_source, view_points);
    std::cout << "view_points:\n" << view_points << "\n";
  }
}

int main(int argc, char** argv)
{
  // seed the random number generator
  std::srand(std::time(0));

  // initialize ROS
  ros::init(argc, argv, "detect_grasps");
  ros::NodeHandle node("~");
  GraspDetectionNode grasp_detection(node);
  grasp_detection.run();

  return 0;
}
