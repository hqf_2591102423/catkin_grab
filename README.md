### 简易机械臂自动抓取程序

#### 使用方法：
  1. source catkin_grasp/devel/setup.bash
  2. 运行“roslaunch arm_base view_arm.launch”启动机械臂硬件接口，arm_base节点报错多为网络IP不对，本机IP应为“192.168.1.135“，ZED连接失败插拔一下ZED端USB Type-C，重启本程序即可，节点正确启动界面：

  ![](./screen.png)

  3. 开启新终端窗口source后运行"roslaunch base_nav base.launch"启动识别和导航节点，机械臂在未识别到目标物体前会往复巡逻，识别后会锁定抓取目标，等待抓取使能。
  4. 开启新终端窗口source后运行"roslaunch gpd_ros ur5.launch"启动gpd节点，效果如下图所示。

  ![](./gpd.png)

  4. (更新) 通过平板功能按钮"enable grip"进行debug，话题"/arm_decision"已移除