# CMake generated Testfile for 
# Source directory: /home/k331/projects/catkin_grab/src
# Build directory: /home/k331/projects/catkin_grab/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("arm_moveit_config")
subdirs("arm_detector")
subdirs("gpd_ros")
subdirs("arm_base")
subdirs("arm_kinematic")
subdirs("base_nav")
subdirs("arm_decision")
subdirs("zed-ros-wrapper/zed-ros-interfaces")
subdirs("zed-ros-wrapper/zed_nodelets")
subdirs("zed-ros-wrapper/zed_ros")
subdirs("zed-ros-wrapper/zed_wrapper")
